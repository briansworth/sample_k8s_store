import logging
import sys
from os import getenv

ENV_VARIABLE = 'DEMO_K8S_STORE_LOG_LEVEL'


def default_logger():
    root = logging.getLogger()
    root.setLevel(getenv(ENV_VARIABLE, 'INFO'))
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)
    return root

