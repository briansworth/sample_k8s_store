import pprint

class AppSecret:
    def __init__(self, name: str, namespace: str, data: str):
        self.name = name
        self.namespace = namespace
        self.data = data

    def __str__(self):
        return self.name

    def __repr__(self):
        return pprint.pformat(self.__dict__)

