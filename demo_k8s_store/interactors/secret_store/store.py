from kubernetes import client, config
from kubernetes.client import V1Namespace, V1ObjectMeta, V1Secret

from .converter import K8sSecretConverter
from demo_k8s_store.entities import AppSecret


class K8sSecretStore:
    def __init__(self, namespace: str):
        self._namespace = namespace

        config.load_kube_config()
        self._client = client.CoreV1Api()
        self._create_namespace_if_not_exists()

        self._converter = K8sSecretConverter()

    @property
    def namespace(self):
        return self._namespace

    def _create_namespace_if_not_exists(self):
        if not self._test_namespace_exists(self.namespace):
            self._create_namespace(self.namespace)

    def _test_namespace_exists(self, namespace: str):
        try:
            exists = self._client.read_namespace(name=namespace)
            return True
        except:
            return False

    def _create_namespace(self, name: str):
        metadata = V1ObjectMeta(name=name)
        body = V1Namespace(metadata=metadata)
        return self._client.create_namespace(body=body)

    def get(self, name: str):
        secret = self._client.read_namespaced_secret(
            name=name, namespace=self.namespace)
        app_secret = self._converter.from_k8s_secret(secret)
        return app_secret

    def delete(self, name: str):
        self._client.delete_namespaced_secret(
            name=name, namespace=self.namespace)

    def set(self, secret: AppSecret):
        if self._test_secret_exists(name=secret.name):
            self.delete(name=secret.name)

        body = self._converter.to_k8s_secret(secret)
        self._client.create_namespaced_secret(
            namespace=self.namespace, body=body)
        # should probably implement rollback if there is a failure
        # Simple variable cache? Design pattern (Command or Memento)?

    def _test_secret_exists(self, name: str):
        try:
            exists = self.get(name=name)
            return True
        except:
            return False

