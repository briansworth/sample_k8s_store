import base64
from pathlib import Path
from typing import Dict

from demo_k8s_store.entities import AppSecret
from demo_k8s_store.utils.config_store import SecretConfigStore


DATA_ENCODING = 'utf-8'


class SecretRetriever:
    def __init__(self, config_file_name: str):
        self._config = SecretConfigStore(config_file_name)
        self._folder = self._get_folder()

    def _get_folder(self):
        root = Path(__file__).parent.parent.parent
        return root.joinpath('secrets')

    def get_all(self):
        secret_list = []
        for sec_id in self._config.list_ids():
            secret = self.get_secret(sec_id)
            secret_list.append(secret)
        return secret_list

    def get_secret(self, secret_id: str):
        sec_conf = self._config.get(secret_id)
        data = self._get_secret_data(file_name=sec_conf['path'])

        app_secret = self._convertto_appsecret(
                secret_config=sec_conf, data=data)
        return app_secret

    def _get_secret_data(self, file_name: str):
        full_path = Path(self._folder).joinpath(file_name)

        with open(full_path) as file:
            content = file.read()
        data = self._encode_secret_data(content)
        return data

    def _encode_secret_data(self, content: str):
        encoded_bytes = base64.b64encode(content.encode(DATA_ENCODING))
        encoded_str = str(encoded_bytes, DATA_ENCODING)
        return {'password': encoded_str}

    def _convertto_appsecret(self, secret_config: Dict, data: str):
        app_secret = AppSecret(
                name=secret_config['name'],
                namespace=secret_config['namespace'],
                data=data)
        return app_secret

