from unittest.mock import Mock

import pytest
from kubernetes import config

from demo_k8s_store.entities import AppSecret
from demo_k8s_store.interactors.secret_store import K8sSecretStore


@pytest.fixture
def mock_appsecret():
    yield AppSecret(name='secret', namespace='test', data={'key': 'Zm9vCg=='})

@pytest.fixture
def mock_config_load(monkeypatch):
    monkeypatch.setattr(config, 'load_kube_config', Mock())

@pytest.fixture
def mock_test_namespace_exists_true(monkeypatch):
    mock_command = Mock()
    mock_command.return_value = True
    monkeypatch.setattr(
        K8sSecretStore, '_test_namespace_exists', mock_command)

@pytest.fixture
def mock_k8sstore(mock_test_namespace_exists_true, mock_config_load):
    store = K8sSecretStore('test')
    yield store

