from kubernetes.client import V1ObjectMeta, V1Secret

from demo_k8s_store.entities import AppSecret


class K8sSecretConverter:
    @classmethod
    def to_k8s_secret(cls, secret: AppSecret) -> V1Secret:
        metadata = V1ObjectMeta(name=secret.name)
        k8s_secret = V1Secret(metadata=metadata, data=secret.data)
        return k8s_secret

    @classmethod
    def from_k8s_secret(cls, secret: V1Secret) -> AppSecret:
        namespace = cls._parse_self_link(
            secret.metadata.self_link, resource_type='namespaces')
        app_secret = AppSecret(
            name=secret.metadata.name,
            namespace=namespace,
            data=secret.data)

        return app_secret

    @classmethod
    def _parse_self_link(cls, self_link: str, resource_type: str):
        resource_list = self_link.split('/')
        type_index = resource_list.index(resource_type)
        return resource_list[(type_index + 1)]

