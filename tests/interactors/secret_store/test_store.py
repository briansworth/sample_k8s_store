from unittest.mock import Mock

import pytest

from demo_k8s_store.interactors.secret_store import K8sSecretStore

@pytest.fixture
def mock_create_namespace(monkeypatch):
    monkeypatch.setattr(K8sSecretStore, '_create_namespace', Mock())


def test_k8ssecretstore_init(monkeypatch, mock_config_load):
    mock_command = Mock()
    monkeypatch.setattr(
        K8sSecretStore, '_create_namespace_if_not_exists', mock_command)

    mock_store = K8sSecretStore('test')

    assert mock_command.call_count == 1

def test_create_namespace_if_not_exists_exists(monkeypatch, mock_k8sstore):
    mock_command = Mock()
    mock_command.return_value = True
    monkeypatch.setattr(
        K8sSecretStore, '_test_namespace_exists', mock_command)

    mock_k8sstore._create_namespace_if_not_exists()

    assert mock_command.call_count == 1

