from pprint import pformat
from time import sleep
from typing import List

from demo_k8s_store.entities import AppSecret
from demo_k8s_store.interactors.retriever import SecretRetriever
from demo_k8s_store.interactors.secret_store import K8sSecretStore
from demo_k8s_store.utils.logger import default_logger

LOG = default_logger()


class DemoSecretInjector:
    _retriever = SecretRetriever('demo_config.json')

    @classmethod
    def execute(cls) -> None:
        all_secrets = cls._retriever.get_all()
        for secret in all_secrets:
            cls._create_secret(secret)

    @classmethod
    def _create_secret(cls, secret: AppSecret) -> None:
        try:
            action = (f'Create secret: Name: [{secret.name}] '
                    'Namespace: [{secret.namespace}]')
            LOG.info(action)
            secret_store = K8sSecretStore(secret.namespace)
            secret_store.set(secret)
        except:
            LOG.error(f'Action: {action}')
            LOG.exception('')

    @classmethod
    def _get_secrets(cls) -> List[AppSecret]:
        secret_list = []
        all_secrets = cls._retriever.get_all()
        for secret in all_secrets:
            sec = cls._get_secret(secret)
            secret_list.append(sec)
        return secret_list

    @classmethod
    def _get_secret(cls, secret: AppSecret) -> AppSecret:
        try:
            action = (f'Get secret: Name: [{secret.name}] '
                    'Namespace: [secret.namespace]')
            LOG.info(action)

            secret_store = K8sSecretStore(secret.namespace)
            return secret_store.get(secret.name)
        except:
            LOG.error(f'Action: {action}')
            LOG.exception('')

    @classmethod
    def _delete_secrets(cls) -> None:
        all_secrets = cls._retriever.get_all()
        for secret in all_secrets:
            cls._delete_secret(secret)

    @classmethod
    def _delete_secret(cls, secret: AppSecret) -> None:
        try:
            action = (f'Delete secret: Name: [{secret.name}] '
                    'Namespace: [{secret.namespace}]')
            LOG.info(action)
            store = K8sSecretStore(secret.namespace)
            store.delete(secret.name)
        except:
            LOG.error(f'Action: {action}')
            LOG.exception('')


if __name__ == "__main__":
    print('\nInjecting secrets into K8s cluster...\n\n')

    DemoSecretInjector.execute()

    print('\n----- Done -----')
    print('\nGetting secrets from K8s cluster...\n\n')

    secrets = DemoSecretInjector._get_secrets()

    print(pformat(secrets))
    print('\n----- Done -----')
    print('\nDeleting secrets from K8s cluster...in 5 seconds:\n\n')
    for i in range(0, 5):
        sleep(1)
        print(4-i)
    print('')

    DemoSecretInjector._delete_secrets()

    print('\n----- Done -----')

