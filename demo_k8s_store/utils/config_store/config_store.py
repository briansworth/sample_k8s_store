import json
from pathlib import Path


class SecretConfigStore:
    def __init__(self, file_name: str):
        self._file_name = file_name
        self._config = self._load_config(file_name)

    def _load_config(self, file_name: str):
        folder = self._get_config_folder()
        file_path = Path(folder).joinpath(file_name)
        with open(file_path) as file:
            return json.load(file)

    def _get_config_folder(self):
        return Path(__file__).parent.parent.parent

    def get(self, secret_id: str):
        for secret in self._config:
            sec_config = secret.get(secret_id, None)
            if sec_config is not None:
                return sec_config

    def list_ids(self):
        id_list = []
        for secret in self._config:
            for k, v in secret.items():
                id_list.append(k)
        return id_list

