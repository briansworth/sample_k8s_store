# Demo K8s Secret Store

## Requirements

Must have access to a Kubernetes cluster.

---

## Setup

---

Would recommend setting up a virtual environment before running any code.
Clone the repository, setup virtualenv, install the package.

```bash
# cd into root of the repo
# install the demo_k8s_store package
python -m pip install -e .
```

## Demo

---

Demonstration of the K8sSecretStore being used in a simple manner:
```python
python ./demo_k8s_store/demo.py
```

Using the SecretRetriever outside of the demo:
```python
from demo_k8s_store.interactors.retriever import SecretRetriever
from demo_k8s_store.interactors.secret_store import K8sSecretStore

# Specify a path to a custom json file containing K8s secret details
retriever = SecretRetriever('demo_config.json')
secrets = retriever.get_all()

# Create all the secrets listed in the demo_config.json file
for secret in secrets:
    # Create a K8s secret store for adding the secrets to a cluster
    secret_store = K8sSecretStore(secret.namespace)
    secret_store.set(secret)

# Check for the secrets after creation
for secret in secrets:
    secret_store = K8sSecretStore(secret.namespace)
    secret_store.get(secret.name)

# Cleanup
for secret in secrets:
    secret_store = K8sSecretStore(secret.namespace)
    secret_store.delete(secret.name)
```

### Directly interact with the K8sSecretStore

```python
from demo_k8s_store.entities import AppSecret
from demo_k8s_store.interactors.secret_store import K8sSecretStore

NAMESPACE = 'test-namespace'
DATA = {'password': 'Rm9vYmFyCg=='}

# Create AppSecret object
secret = AppSecret(name='sample', namespace=NAMESPACE, data=DATA)

# Intialize the store to a test namespace
store = K8sSecretStore(NAMESPACE)

# Create the AppSecret in the namespace
store.set(secret)

# Check the AppSecret in the namespace
store.get(secret.name)

# Overwrite secret
secret.data = {'new_password': 'Zml6emJ1enoK'}
store.set(secret)

store.get(secret.name)

# Delete
store.delete(secret.name)
```


