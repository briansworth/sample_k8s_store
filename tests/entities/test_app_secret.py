import pytest

from demo_k8s_store.entities import AppSecret


def test_appsecret_str(mock_appsecret):
    result = str(mock_appsecret)

    assert result == mock_appsecret.name

def test_appsecret_repr(mock_appsecret):
    result = repr(mock_appsecret)

    assert result.find(mock_appsecret.name) > -1
    assert result.find(mock_appsecret.namespace) > -1

